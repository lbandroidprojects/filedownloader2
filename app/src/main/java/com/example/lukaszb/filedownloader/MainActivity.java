package com.example.lukaszb.filedownloader;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;



public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button bDownload;
    private ProgressBar pb;
    private final static int EXTERNAL_STORAGE_PERMISSION=17;
    private final static String URL="http://alex.smola.org/drafts/thebook.pdf";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bDownload=findViewById(R.id.bDownload);
        bDownload.setOnClickListener(this);
        pb=findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View v) {
       if(isNetworkActive()){
          if(ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED){
              DownloadTask dt=new DownloadTask();
              dt.execute(URL);
             }
        else
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},EXTERNAL_STORAGE_PERMISSION);
       }else
       {
           Toast.makeText(this,"Brak połączenia sieciowego",Toast.LENGTH_SHORT).show();
       }
    }

    private boolean isNetworkActive(){
        boolean active=false;
        ConnectivityManager cm=
                (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni=cm.getActiveNetworkInfo();
        active=ni!=null && ni.isAvailable() && ni.isConnected();
        return active;
    }

    class DownloadTask extends AsyncTask<String,Integer,DownloadInfo>{
        private String path;
        private long totalSize=0;
        private String fileName;
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected DownloadInfo doInBackground(String... strings) {
            path=strings[0];
            fileName=extractFileName();
            long startTime=0,stopTime=0;
            try {
                URL url = new URL(path);
                HttpURLConnection connection=(HttpURLConnection) url.openConnection();
                totalSize=connection.getContentLength();
                try(InputStream is=connection.getInputStream();
                    FileOutputStream fos=new FileOutputStream(getLocalPath()+"/"+fileName)){
                    long currentSize=0;
                    int bufSize=2048,c;
                    startTime=System.currentTimeMillis();
                    byte[] buffer=new byte[bufSize];
                    while((c=is.read(buffer,0,bufSize))>-1){
                        fos.write(buffer,0,c);
                        currentSize+=c;
                        int procent=(int)(((double)currentSize)/((double)totalSize)*100);
                        publishProgress(procent);
                    }
                    stopTime=System.currentTimeMillis();
                }
            }catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new DownloadInfo(totalSize,(int)(stopTime-startTime));
        }


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            pb.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(DownloadInfo downloadInfo) {
            super.onPostExecute(downloadInfo);
            Toast.makeText(getApplicationContext(),"Czas ładowania: "+(downloadInfo.getDownloadTime()/1000)+" sekund, rozmiar: "
            +downloadInfo.getFileSize()+" bajtów",Toast.LENGTH_SHORT).show();
        }

        private String getLocalPath(){
            return Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath().toString();
        }

        private String extractFileName(){
            return path.substring(path.lastIndexOf("/")+1) ;
        }
    }

    class DownloadInfo{
        private long fileSize;
        private int downloadTime;
        public DownloadInfo(long fileSize,int downloadTime){
            this.fileSize=fileSize;
            this.downloadTime=downloadTime;
        }

        public long getFileSize() {
            return fileSize;
        }

        public int getDownloadTime() {
            return downloadTime;
        }
    }
}
